/*
 * Copyright (C) 2012 Matt Broadstone
 * Contact: http://bitbucket.org/devonit/qconnman
 *
 * This file is part of the QConnman Library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */

#ifndef TECHNOLOGY_H
#define TECHNOLOGY_H

#include <QObject>
#include <QString>
#include <QDBusVariant>

#include "connmanobject.h"

class TechnologyInterface;
class Technology : public ConnManObject
{
    Q_OBJECT
    Q_PROPERTY(bool Powered READ isPowered WRITE setPoweredInternal NOTIFY poweredChanged)
    Q_PROPERTY(bool Connected READ isConnected WRITE setConnectedInternal NOTIFY dataChanged)
    Q_PROPERTY(QString Name READ name WRITE setNameInternal NOTIFY dataChanged)
    Q_PROPERTY(QString Type READ type WRITE setTypeInternal NOTIFY dataChanged)
    Q_PROPERTY(bool Tethering READ tetheringAllowed WRITE setTetheringAllowedInternal NOTIFY dataChanged)
    Q_PROPERTY(QString TetheringIdentifier READ tetheringIdentifier WRITE setTetheringIdentifierInternal NOTIFY dataChanged)
    Q_PROPERTY(QString TetheringPassphrase READ tetheringPassphrase WRITE setTetheringPassphraseInternal NOTIFY dataChanged)
public:
    explicit Technology(const QDBusObjectPath &path, const QVariantMap &properties, QObject *parent = 0);
    ~Technology();

    QDBusObjectPath path() const;
    QString name() const;
    QString type() const;
    bool isConnected() const;

    bool isPowered() const;
    void setPowered(bool powered);

    bool tetheringAllowed() const;
    void setTetheringAllowed(bool allowed);

    QString tetheringIdentifier() const;
    void setTetheringIdentifier(const QString &identifier);

    QString tetheringPassphrase() const;
    void setTetheringPassphrase(const QString &passphrase);

Q_SIGNALS:
    void dataChanged();
    void poweredChanged(bool powered);

private:
    void setPoweredInternal(bool powered);
    void setConnectedInternal(bool connected);
    void setNameInternal(const QString &name);
    void setTypeInternal(const QString &type);
    void setTetheringAllowedInternal(bool allowed);
    void setTetheringIdentifierInternal(const QString &identifier);
    void setTetheringPassphraseInternal(const QString &passphrase);


    TechnologyInterface *m_technologyInterface;

    QDBusObjectPath m_path;
    bool m_powered;
    bool m_connected;
    QString m_name;
    QString m_type;
    bool m_tethering;
    QString m_tetheringIdentifier;
    QString m_tetheringPassphrase;

};
Q_DECLARE_METATYPE(Technology*)

#endif

